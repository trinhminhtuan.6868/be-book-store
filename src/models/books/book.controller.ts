import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Book } from './book.entity';
import { BookService } from './book.service';

@Controller('books')
export class BookController {
  constructor(private readonly bookService: BookService) {}

  @Get()
  findAll(): Promise<Book[]> {
    return this.bookService.findAll();
  }

  @Get(':id')
  findOne(@Param() params) {
    return this.bookService.findOne(params.id);
  }

  @Post()
  create(@Body() book: Book) {
    return this.bookService.createNewBook(book);
  }

  @Put(':id')
  update(@Body() book: Book, @Param() params) {
    return this.bookService.update(book, params.id);
  }

  @Delete(':id')
  deleteUser(@Param() params) {
    return this.bookService.delete(params.id);
  }
}
