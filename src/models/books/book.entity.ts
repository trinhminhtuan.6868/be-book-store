import { CoverTypeEnum } from './../../constants/common';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Book {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 255, nullable: true })
  original_title: string;

  @Column({ length: 255 })
  title: string;

  @Column({ type: 'float' })
  price: number;

  @Column({ length: 255 })
  issuing_company: string;

  @Column({ type: 'float', nullable: true })
  height: number;

  @Column({ type: 'float', nullable: true })
  width: number;

  @Column({ type: 'int' })
  pages: number;

  @Column({
    type: 'enum',
    enum: [CoverTypeEnum.HARD_COVER, CoverTypeEnum.PAPER_BACK],
    default: CoverTypeEnum.PAPER_BACK,
  })
  cover_type: CoverTypeEnum;

  @Column({ length: 255 })
  publisher: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  publish_date: Date;

  @Column({ type: 'text' })
  description: string;

  // @Column({ type: 'blob' })
  // cover_image: string;

  @Column({ type: 'float', nullable: true })
  rate: number;

  @Column({ length: 255 })
  translator: string;
}
