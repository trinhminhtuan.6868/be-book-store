import { Injectable } from '@nestjs/common';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import {
  DataSource,
  DeleteResult,
  InsertResult,
  Repository,
  UpdateResult,
} from 'typeorm';
import { Book } from './book.entity';

@Injectable()
export class BookService {
  constructor(
    @InjectRepository(Book)
    private readonly bookRepo: Repository<Book>,

    @InjectDataSource() private dataSource: DataSource,
  ) {}

  async findAll(): Promise<Book[]> {
    return this.dataSource.query(`SELECT * FROM book`);
  }

  async findOne(id): Promise<Book> {
    return await this.dataSource
      .getRepository(Book)
      .createQueryBuilder()
      .where('id = :id', { id })
      .getOne();
  }

  async createNewBook(book: Book): Promise<InsertResult> {
    return await this.dataSource
      .createQueryBuilder()
      .insert()
      .into(Book)
      .values({ ...book })
      .execute();
  }

  async update(book: Book, id: number): Promise<UpdateResult> {
    return await this.dataSource
      .createQueryBuilder()
      .update(Book)
      .set({
        ...book,
      })
      .where('id = :id', { id })
      .execute();
  }

  async delete(id): Promise<DeleteResult> {
    return await this.bookRepo.delete(id);
  }
}
