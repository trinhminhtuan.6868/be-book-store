export enum CoverTypeEnum {
  PAPER_BACK = 'PAPER_BACK',
  HARD_COVER = 'HARD_COVER',
}
