import { OrmModule } from './common/orm/orm.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BookModule } from './models/books/book.module';

@Module({
  imports: [BookModule, OrmModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
